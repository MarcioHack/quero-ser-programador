Descreva porque você acha que seria um bom programador:

* Primeiramente, na minha opinião, o ramo da Programação é **um dos grandes mercados que tendem a evoluir cada vez mais** juntamente com o
crescimento tecnológico que está também em ascensão. E isso, me motiva a ingressar neste ramo e adquirir mais este conhecimento.
Em um código, precisamos ser detalhistas, para que possamos construir algo que seja legível (na liguagem de programação), onde outros
desenvolvedores "botem o olho" e compreendam o que ali está programado (e o criador também, é claro). E me considero uma pessoa detalhista,
que se necessário analisar algum script (falando de dashboards Syonet por exemplo), se for preciso olhar linha a linha para identificar
onde está a inconsistência, assim faço.

* Para concluir, é muito gratificante saber que, em caso de sucesso nesta nova etapa, estarei adquirindo mais um conhecimento. Algo que
tende a crescer, além de estar agregando pontos em meu crescimento profissional.

Abaixo, um demonstrativo do que deve ser feito para se tornar um bom programador:

| Segunda | Terça | Quarta | Quinta | Sexta | Sábado | Domingo |
| :------:|:-----:|:------:|:------:|:-----:|:------:|:-------:|
| Estudar e Praticar | Estudar e Praticar | Estudar e Praticar | Estudar e Praticar | Estudar e Praticar | Estudar e Praticar | Se divertir MUITO! Mas claro, depois de estudar e Praticar |

<dl>
	<dt>Os 5 principais requisitos para se tornar um bom programador:</dt>
	<dd>1. Ter bom raciocínio lógico;</dd>
	<dd>2. Ser autodidata;</dd>
	<dd>3. Gostar de resolver problemas;</dd>
	<dd>4. Saber Inglês;</dd>
	<dd>5. Gostar de aprender
</dl>

*Fonte:* https://www.tiagogouvea.com.br/os-5-principais-requisitos-para-ser-um-bom-programador
